# Week 1 #

During the first week of the course I installed unity on my personal laptop and set myself up with a Unity account.  
The first thing in unity that was shown to me was how to locate the asset store where we are able to get free assets for our projects.  
Also during the first week of the course I started out by just getting myself familiar with the layout of unity and how to create simple things such as terrain.  
`This is done by right clicking in the hierarchy > 3D Object > Terrain`  
Then I learned how to add diffent texture to the terrain.  
`Click on Terrain asset in hierarchy > Go to Paint the terrain texture > Edit Texture`  
After this I played around with adding assets downloaded from the asset store onto the terrain and learned the cameral controls. 

---

# Week 2 #

During the second week of the course Jacob instructed us on how to install the Steam VR pack from the asset store required to use the project with a VR headset.  
He also showed us that we needed to add the camera rig prefab to the scene.  
`Note when adding the camera rig prefab to the scene it needs to be level with the terrain or an asset`  
Jacob also showed us how to add the teleport script from the I: drive onto the camera rig eyes in order to use the teleport method of movement.  
Further to this I went through the raycasting tutorial on the Unity website in order to understand how it worked as it is an important aspect of VR.

---

# Week 3 #

During week three Jacob showed us the procedure of adding the VRTK package and went through the steps to install the simulator into the project so we can test without using the Vive.  
`See Project SJR to find required components to be added to make simulator function correctly`  
Jacob also showed us how to implement the trackpad movement system `NOTE DO NOT USE THIS FOR MOVEMENT IT IS HORRIBLE`  
During week three I added some gun prefabs into the test scene of our project and tried to make them interactable. I was unsuccessful in doing so.  
Jacob pointed me in the direction of the VRTK examples in order to solve the issue I had with interating with the gun.  
`Example 05 provides instructions for grabbing objects`  
I then dicovered issues with the object facing weird directions. Jacob also advised to go to examples in VRTK for help.  
`Example 15 provides instructions to get objects such as guns to snap in the direction the player is facing.`

---